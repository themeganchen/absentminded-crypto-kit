#include <obliv.oh>
#include "oram.oh"
#include <copy.oh>
#include "test_generic.h"

#define SAMPLES 30

static const char TESTNAME[] = "oram_init_benchmark";

#define TEXT_HELP_SUPPLEMENTARY "\
  -e \x1b[4mNUMBER\x1b[0m, --element-count=\x1b[4mNUMBER\x1b[0m \n\t\tsearch through \x1b[4mNUMBER\x1b[0m elements\n\n\
  -s \x1b[4mNUMBER\x1b[0m, --element-size=\x1b[4mNUMBER\x1b[0m \n\t\tperform \x1b[4mNUMBER\x1b[0m searches on the same data\n\n\
  -o \x1b[4mTYPE\x1b[0m, --oram-type=\x1b[4mTYPE\x1b[0m \n\t\tforce all ORAMs to be \x1b[4mTYPE\x1b[0m ORAMs. Valid types are \033[1msqrt\033[0m, \033[1mcircuit\033[0m, and \033[1mlinear\033[0m.\n\n\
  -i \x1b[4mNUMBER\x1b[0m, --samples=\x1b[4mNUMBER\x1b[0m \n\t\trun \x1b[4mNUMBER\x1b[0m iterations of the benchmark\n\n"

static const char options_string[] = "e:s:o:i:";
static struct option long_options[] = {
	{"element-count", required_argument, NULL, 'e'},
	{"element-size", required_argument, NULL, 's'},
	{"oram-type", required_argument, NULL, 'o'},
	{"samples", required_argument, NULL, 'i'},
	{0, 0, 0, 0}
};

char* get_test_name() {
	return TESTNAME;
}

char* get_supplementary_options_string() {
	return options_string;
}

struct option* get_long_options() {
	return long_options;
}

void print_supplementary_help() {
	fprintf(stderr, TEXT_HELP_SUPPLEMENTARY);
}

void test_main(void*varg) {

	#ifdef ORAM_OVERRIDE
	oram_set_default_type(ORAM_OVERRIDE);
	#endif

	int elct = 4;
	int elsz = 1;
	int samples = 1;

	args_t * args_pass = varg;
	int arg;
	optind = 0; // this allows us to getopt a second time
	while ((arg = getopt_long(args_pass->argc, args_pass->argv, options_string, long_options, NULL)) != -1) {
		if (arg == 'e') {
			elct = atoi(optarg);
			if (elct <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		} else if (arg == 's') {
			elsz = atoi(optarg);
			if (elsz <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		} else if (arg == 'o') {
			if (strcmp(optarg,"sqrt") == 0) {
				oram_set_default_type(ORAM_TYPE_SQRT);
			} else if (strcmp(optarg,"circuit") == 0) {
				oram_set_default_type(ORAM_TYPE_CIRCUIT);
			} else if (strcmp(optarg,"linear") == 0) {
				oram_set_default_type(ORAM_TYPE_LINEAR);
			} else {
				fprintf (stderr, "Invalid argument for -%c.\n", arg);
				return;
			}
		} else if (arg == 'i') {
			samples = atoi(optarg);
			if (samples <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		} else if (arg == '?' || arg == ':') {
			if (optopt == 'e' || optopt == 's' || optopt == 'o' || optopt == 'i') {
				fprintf (stderr, "Option -%c requires an argument.\n", optopt);
				return;
			} else {
				fprintf (stderr, "Option -%c not recognized.\n", optopt);
				return;
			}
		} else {
			abort();
		}
	}

	fprintf(stdout, "# ORAM INIT (elct, elsz, sample 1, sample 2, ... sample n)\n");

	OcCopy cpy = ocCopyIntN(elsz);

	uint64_t tally = 0;
	obliv uint32_t * input = calloc(elsz * elct, sizeof(obliv uint32_t));

	fprintf(stdout, "%d,%d", elct, elsz);

	for (int kk = 0; kk < samples; kk++) {
		for (int kkkkk = 0; kkkkk < (elsz * elct); kkkkk++) input[kkkkk] = feedOblivInt(rand(), 1);
		uint64_t startTime = current_timestamp();
		oram * o = oram_from_array(ORAM_TYPE_AUTO, &cpy, elct, input);
		uint64_t runtime = current_timestamp() - startTime;

		fprintf(stdout, ",%llu", runtime);
		fflush(stdout);
		tally += runtime;

		oram_free(o);
	}

	free(input);
	fprintf(stdout, "\n");
	fprintf(stderr, "Init (count:%d, size: %d): %llu microseconds avg\n", elct, elsz, tally / samples);

}
